//listen for form submit
document.getElementById("myForm").addEventListener("submit", saveBookmark);

//save Bookmark
function saveBookmark(e) {
  //get form values
  var siteName = document.getElementById("siteName").value;
  var siteUrl = document.getElementById("siteUrl").value;

  var bookmark = {
    name: siteName,
    url: siteUrl
  };

  // //Local storage Test
  // localStorage.setItem("test", "Helloworld");
  // console.log(localStorage.getItem("test"));

  //test if bookmarks is null
  if (localStorage.getItem("bookmarks") === null) {
    // init array
    var bookmarks = [];
    // add to array
    bookmarks.push(bookmark);
    // set to localstorage
    localStorage.setItem("bookmarks", JSON.stringify(bookmarks));
  } else {
    //get bookmarks from localstorage
    var bookmarks = JSON.parse(localStorage.getItem("bookmarks"));
    // add bookmark to array
    bookmarks.push(bookmark);
    // re-set back to localstorage
    localStorage.setItem("bookmarks", JSON.stringify(bookmark));
  }

  //prevent form from submitting
  e.preventDefault();
}
// fetch bookmarks
function fetchBookmarks() {
  //get bookmarks from localstorage
  var bookmarks = JSON.parse(localStorage.getItem("bookmarks"));
  // get output id
  var bookmarkResult = document.getElementById("bookmarkResult");
  //build output
  bookmarkResult.innerHTML = "";
  for (var i = 0; i < bookmarks.length; i++) {
    var name = bookmarks[i].name;
    var url = bookmarks[i].url;

    bookmarkResult.innerHTML += name;
  }
}
